# AquariumCooler
This is the ESP8266 firmware used to control a fans power depending on a temperature.

## Features
* Temperature reading via Dallastemperature/OneWire
* OLED display shows current temperature, temperature threshold and fan state
* Two button inputs allow threshold edditing
* One relay output to toggle the fan 

## Images
![Prototype](https://files.mastodon.social/media_attachments/files/106/430/847/263/333/087/original/4d267830dca9d2b1.jpg "Prototype")
![Finished cb](https://files.mastodon.social/media_attachments/files/106/432/114/298/169/135/original/a10001e342a61bda.jpg "Finished cirbuit board")