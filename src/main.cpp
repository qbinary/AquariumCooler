/*
 *    AquariumCooler - Software to toggle a fan depending on temperature
 *    Copyright (C) 2022  qbin
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#include <OneWire.h>
#include <DallasTemperature.h>

// OLED
// > SCL D1; SDA D2

const int SCREEN_WIDTH = 128; // OLED display width,  in pixels
const int SCREEN_HEIGHT = 64; // OLED display height, in pixels

const int SETTING_UP_PIN = D6;
const int SETTING_DOWN_PIN = D7;
const int SETTING_MODE_PIN = D8;

const int RELAY_PIN = D5;
const int SENSOR_PIN = D3;

// Screen
Adafruit_SSD1306 oled(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

// Sensor
OneWire oneWire(SENSOR_PIN);
DallasTemperature tempSensor(&oneWire);

double current = 0;
double setting = 25;

bool fanPower = false;

int timer = 100;

void bDisplaySpacer()
{
    oled.setTextSize(1);
    oled.println();
}

void bDisplayData(String title, String data, bool big)
{
    oled.setTextSize(1);
    oled.println(title);

    bDisplaySpacer();

    if (big)
        oled.setTextSize(2);
    else
        oled.setTextSize(1);
    oled.print("> ");
    oled.print(data);
    oled.println("C");

    bDisplaySpacer();
}

void showCooling(bool show)
{
    if (show)
    {
        oled.setCursor(SCREEN_WIDTH - 42, SCREEN_HEIGHT - 8);
        oled.setTextSize(1);
        oled.print("cooling");
    }
}

void displayUpdate(String current, String setting)
{
    oled.clearDisplay();

    oled.setTextColor(WHITE);
    oled.setCursor(0, 0);

    bDisplayData("Current", current, true);
    bDisplayData("Setting", setting, false);

    showCooling(fanPower);

    oled.display();
}

void setup()
{
    Serial.begin(9600);
    Serial.println();

    pinMode(SETTING_UP_PIN, INPUT_PULLUP);
    pinMode(SETTING_DOWN_PIN, INPUT_PULLUP);

    pinMode(RELAY_PIN, OUTPUT);

    // initialize OLED display with address 0x3C for 128x64
    if (!oled.begin(SSD1306_SWITCHCAPVCC, 0x3C))
    {
        Serial.println(F("SSD1306 allocation failed"));
        while (true)
            ;
    }

    tempSensor.begin();
    displayUpdate("-", "-");
}

void loop()
{
    // Check buttons
    if (digitalRead(SETTING_UP_PIN) == LOW)
        setting+=0.25;
    else if (digitalRead(SETTING_DOWN_PIN) == LOW)
        setting-=0.25;

    if (timer % 100 == 0)
    {
        oled.drawPixel(SCREEN_WIDTH - 1, 0, WHITE);
        oled.display();

        // Update sensor
        tempSensor.requestTemperatures();
        current = tempSensor.getTempCByIndex(0);

        // Determine fan state
        if (current >= setting)
            fanPower = true;
        else if (fanPower && current + 0.1 <= setting)
            fanPower = false;

        // Set fan state
        if (fanPower)
            digitalWrite(RELAY_PIN, HIGH);
        else
            digitalWrite(RELAY_PIN, LOW);
    }

    // Update display
    displayUpdate(String(current), String(setting));

    delay(100);
    timer++;
}